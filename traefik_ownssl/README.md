# docker-traefik-ownssl
## Date 
08-11-2021 first creation
# Description 

### Traefik 
Traefik est un edge routeur open source permettant de gérer les container en y ajoutant des routes, middleware(règles), etc.. le tout en dynamique sans reload de sa configuration.
Pour plus d'information : https://doc.traefik.io/traefik/

### La stack

Cette stack permet de run Traefik en lui donnant la gestion de vos propres certificats SSL. 
Ces derniers seront distribués sur l'ensemble de vos services gérés par Traefik. 


## Fonctionnement

### Le docker-compose.yml

* userns_mode:'host' permet de lancer le container en tant que l'user de l'hote(si vous travaillez avec les usernames spaces) 
* les classiques ports 80 - 443  et le 8080 qui servira pour l'acces au dashboard web
* les commands nous permettent d'affiner nos préférences(voir le compose pour plus d'informations)
* Traefik doit avoir acces a la socket docker (mais vous pouvez travailler diffemment ici en readaptant: https://gitlab.com/steve.decot/docker-traefik-protect-socket). Les fichiers de conf traefik et le dossier "certs" seront montes dans le volume
* Dans la partie label, le middlewares d'auth a ete commente. Par defaut, le dashboard sera **accessible** sans authentification. (si vous souhaitez activer une authentification, passez au point suivant)
* le network "web" sera utilise par traefik ainsi que *tous les futurs service derriere Traefik* 

#### Securiser le dashboard

Desactivez les deux lignes suivantes dans la partie label de votre compose comme suit :

```bash
- "traefik.http.routers.api.middlewares=auth"
- "traefik.http.middlewares.auth.basicauth.users=your_user:your_passwd"
```

La premiere ligne est l'activation du middlewares de type "auth". L'authentification etant declaree, il faut lui founir le couple login/passwd

L'utilisateur sera en clair, cependant le passwd doit etre fourni chiffre et non en *clair*

Vous pouvez utiliser deux methodes 

* Via le module htpasswd d'apache

```bash
sudo apt install apache2-utils
htpasswd -nB steve
```

* Via un container qui utilise le module htpasswd d'apache et vous retournera votre passwd chiffre : https://hub.docker.com/r/xmartlabs/htpasswd

```bash
docker run --rm -ti xmartlabs/htpasswd steve devops > htpasswd
cat htpasswd
steve:$2y$05$UgVCIesebUJe60vBk0gfDeewv4t.mEZro2Bru7RzEe22jbS7CCmsK 
```

Dans notre exemple ci-dessus, mon mot de passe "devops" a ete chiffre et stocke dans le fichier htpasswd. Vous remarquerez que ce dernier contient des "$". Pour que Traefik interprete ces "$", il faut les doubler imperativement auquel cas le mot de passe ne sera pas bon. 

> Exemple 
```bash
- "traefik.http.routers.api.middlewares=auth"
- "traefik.http.middlewares.auth.basicauth.users=steve:$$2y$$05$$UgVCIesebUJe60vBk0gfDeewv4t.mEZro2Bru7RzEe22jbS7CCmsK"
```

### Les fichiers *.toml / config Traefik 

Il y a plusieurs facon de proceder mais celle qui est adoptee ici est la suivante :
* Le fichier global de configuration de traefik -- > traefik.toml
* Le fichier "dynamic.toml" qui est celui contenant le path de vos certificats SSL
  
#### traefik.toml 
Nous activons le dashboard et desactivons l acces a l'api. 
Ensuite nous forcons toutes requete HTTP en HTTPS. 
Nous lui defissons un provider.file qui est simplement le dynamic.toml
Enfin nous lui definissons le provider docker qui permet la lecture des label et la definition de notre network "web"

#### dynamic.toml 
Contient simplement nos path pour nos fichiers SSL.
* .crt
* .key

