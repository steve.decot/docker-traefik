# docker-traefik-protect-socket
## Description

Traefik attaque la socket docker en TCP sur un HaProxy. Cela limite l'accès à la socket à ce que l'on a besoins. Dans ce repos, l'accès au status des containers est suffisante ( ~ docker ps)

Le fichier .toml de Traefik est modifié pour pouvoir accéder en TCP sur le HaProxy, on y rajoute dans le provider ceci :

``` toml
[providers]
  [providers.docker]
    watch = true
    exposedByDefault = false
    endpoint= "tcp://dockerproxy:2375"
    network = "web"
```
Ce haProxy à quant à lui un accès limité et modulaire. 
Il pourra uniquement lister des containers. 

Le HAproxy est une image Alpine officielle basée sur HaProxy avec des modifications spécifiques.
 
[Pour en savoir plus sur ce projet c'est ici](https://github.com/Tecnativa/docker-socket-proxy#grant-or-revoke-access-to-certain-api-sections)

L'accès définit par défaut ici est le listing des containers ( docker ps). C'est amplement suffisant pour Traefik. 
Tout se joue via variable d'environment dans le compose comme suit : 

```
environment:
      - CONTAINERS=1 # = 1 pour activer, 0 pour au contraire le désactiver
```

Voici les différentes actions qui peuvent être implémentée
- BUILD
- COMMIT
- CONFIGS
- CONTAINERS
- DISTRIBUTION
- EXEC
- IMAGES
- INFO
- NETWORKS
- NODES
- PLUGINS
- SERVICES
- SESSION
- SWARM
- SYSTEM
- TASKS
- VOLUMES


## TLS  

Concernant le TLS, veuillez lire le fichier ACME.LISMOI. 

## Traefik CONFIG
Pour générer votre couple login/passwd pour l'api de traefik : 

``` bash
docker pull xmartlabs/htpasswd
docker run --rm -ti xmartlabs/htpasswd monLogin monPasswd > htpasswd
cat htpasswd
```



links :

    https://medium.com/@containeroo/traefik-2-0-paranoid-about-mounting-var-run-docker-sock-22da9cb3e78c
    https://www.grottedubarbu.fr/securiser-socket-docker/
