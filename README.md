# docker-traefik
## Révision au 11-12-2022
## Description
Traefik est un edge routeur open source permettant de gérer les container en y ajoutant des routes, middleware(règles), etc.. le tout en dynamique sans reload de sa configuration.
Pour plus d'information : https://doc.traefik.io/traefik/
## Contenu des stacks de ce repos
### docker-traefik-ownssl
Cette stack vous permettra de lancer votre instance Traefik avec vos propres certificats TLS. 
Voir le readme du dossier pour plus d'informations.
### docker-traefik-protect-socket
Cette stack est couplée avec un HAproxy. Traefik ne possède pas de droits direct sur la socket docker et seul le service HAproxy peut affiner ces derniers. 
Voir le readme du dossier pour plus d'informations.  
### docker-traefik-letsencrypt
Traefik avec sa configuration pour letsencrypt.

## Liens
* https://doc.traefik.io/traefik/
* https://hub.docker.com/_/traefik
