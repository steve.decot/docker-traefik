# Traefik with letsencrypt
## Description 
Traefik sera fonctionnel avec letsencrypt. Le renouvellement du certificat se fera automatiquement. 

## User / Pwd dashboard
Si vous activez le dashboard, ce dernier doit être sécurisé via la commande htpasswd d'apache2. 
Voici un exemple de création via un container contenant httpasswd
´´´bash=
docker run --rm -ti xmartlabs/htpasswd your-dasboard-user your-pwd-dashboard > htpasswd
´´´
Copiez-collez ensuite le contenu d'htpasswd qui vient d etre cree. Doublez les "$" sinon le passwd que vous avez entré ne fonctionnera pas. 

## Permissions pour la socket docker
Les permissions sont en RO pour limiter l acces de traefik. Nous ajoutons dans le compose ´´´ userns_mode: 'host' ´´´ . Affinez vos ownership en fonction de l'utilisateur qui execute le compose sur la socket docker. 

## Fichiers 
* acme.json : Contenant pour letsencryp. Doit etre créé *avant* le premier run avec les bonnes permissions (dependant de l utilisateur defini dans le daemon.json de docker)
* trafik.toml : Est la configuration que Traefik suivra. Idem pour les permissions que le acme.json
